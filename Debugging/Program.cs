﻿using Servr.HTTP;
using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;

namespace testing
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Controller test = new Controller();
            test.AddModule(new Servr.HTTP.Modules.Static.Processor());
            test.AddModule(new Servr.HTTP.Modules.Rewrite.Processor(new Servr.HTTP.Modules.Rewrite.Rule[]
                {
                    new Servr.HTTP.Modules.Rewrite.Rule()
                    {
                        Match = "/test/.*",
                        Condition = delegate(Regex match, ref Request requests)
                        {
                            return true;
                        },
                        Replace = delegate(Regex match, ref Request request)
                        {
                            return $"http://google.com/";
                        },
                        ExternalRedirect = true,
                        StatusCode = StatusCodes.TemporaryRedirect
                    }
                }));
            test.Listen(new IPAddress(new byte[] { 127, 0, 0, 1 }), 1234);

            Console.WriteLine("Server started, press ctrl+C to exit");
            ManualResetEvent wait = new ManualResetEvent(false);
            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e)
            {
                e.Cancel = true;
                wait.Set();
            };

            wait.WaitOne();

            Console.WriteLine("Server stopped");
        }
    }
}