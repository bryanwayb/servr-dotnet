﻿using System.Net.Security;
using System.Net.Sockets;

namespace Servr.Core.Handler
{
    /// <summary>
    /// Interfaces with encrypted Socket connections.
    /// </summary>
    public class SecureSocketHandle : SocketHandle
    {
        /// <summary>
        /// The NetworkStream object that runs over the Socket connection.
        /// </summary>
        public NetworkStream Network;

        /// <summary>
        /// The SslStream object for reading and writing encrypted data over a Socket.
        /// </summary>
        public SslStream SSL;

        /// <summary>
        /// Creates a new SecureSocketHandle object.
        /// </summary>
        /// <param name="sock">The Socket object to use for this connection.</param>
        /// <param name="certCallback">A callback to use for cert authentication. Can be null.</param>
        /// <param name="localCertCallback">A callback to use for local cert authentication. Can be null.</param>
        public SecureSocketHandle(Socket sock, RemoteCertificateValidationCallback certCallback, LocalCertificateSelectionCallback localCertCallback) : base(sock)
        {
            Network = new NetworkStream(sock);
            SSL = new SslStream(Network, false, certCallback, localCertCallback);
        }

        public override int Read(byte[] buffer, int offset = 0, int readMin = -1)
        {
            int readBytes = offset,
                remainingBytes = buffer.Length - readBytes;
            if (readMin == -1)
            {
                readMin = remainingBytes;
            }
            while (readMin > 0)
            {
                int i = SSL.Read(buffer, readBytes, remainingBytes);
                if (i == 0)
                {
                    break;
                }
                readBytes += i;
                readMin -= i;
                remainingBytes -= i;
            }
            return readBytes;
        }

        public override int Write(byte[] buffer)
        {
            SSL.Write(buffer);
            return buffer.Length;
        }

        protected override void Dispose(bool disposing)
        {
            if (!DisposedStatus)
            {
                if (disposing)
                {
                    if (SSL != null)
                    {
                        SSL.Dispose();
                        SSL = null;
                    }
                    if (Network != null)
                    {
                        Network.Dispose();
                        Network = null;
                    }
                }

                DisposedStatus = true;
            }
        }
    }
}