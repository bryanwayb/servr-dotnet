﻿using System;
using System.Diagnostics;
using System.Net.Sockets;

namespace Servr.Core.Handler
{
    /// <summary>
    /// Interfaces with Socket connections.
    /// </summary>
    public class SocketHandle : IDisposable
    {
        /// <summary>
        /// The underlying Socket object that is used for this handle.
        /// </summary>
        public Socket Sock;

        /// <summary>
        /// Creates a new SocketHandle object.
        /// </summary>
        /// <param name="sock">The Socket object to use for this connection.</param>
        public SocketHandle(Socket sock)
        {
            Debug.Assert(sock != null, "A Socket object must be supplied");

            Sock = sock;
        }

        protected bool DisposedStatus = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!DisposedStatus)
            {
                if (Sock != null)
                {
                    Sock.Dispose();
                }
            }
        }

        /// <summary>
        /// Read from the connection into a buffer.
        /// </summary>
        /// <param name="buffer">The buffer to read into.</param>
        /// <param name="offset">The location in the buffer to being reading into.</param>
        /// <param name="readMin">The minimum number of bytes to read before returning.</param>
        /// <returns>The number of bytes actually read from the connection.</returns>
        public virtual int Read(byte[] buffer, int offset = 0, int readMin = -1)
        {
            int readBytes = offset,
                remainingBytes = buffer.Length - readBytes;
            if (readMin == -1)
            {
                readMin = remainingBytes;
            }
            while (readMin > 0)
            {
                int i = 0;
                try
                {
                    i = Sock.Receive(buffer, readBytes, remainingBytes, SocketFlags.None);
                }
                catch (SocketException) { }

                if (i == 0)
                {
                    break;
                }
                readBytes += i;
                readMin -= i;
                remainingBytes -= i;
            }
            return readBytes;
        }

        /// <summary>
        /// Writes the contents of a buffer to the connection.
        /// </summary>
        /// <param name="buffer">The buffer to send.</param>
        /// <returns>The number of bytes written.</returns>
        public virtual int Write(byte[] buffer)
        {
            return Sock.Send(buffer);
        }
    }
}