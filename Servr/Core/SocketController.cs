﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Servr.Core
{
    /// <summary>
    /// The base SocketController class that handles the basis of server functions.
    /// </summary>
    public abstract class SocketController
    {
        /// <summary>
        /// Returns true if this SocketController is currently listening for new connections.
        /// </summary>
        public bool IsListening = false;

        /// <summary>
        /// The number of current connections that are being processed.
        /// </summary>
        public ulong Connections = 0;

        /// <summary>
        /// To be overridden. Called when conections have been initialized and are ready for sending/receiving data.
        /// </summary>
        /// <param name="handle">The SocketHandle to the new connection.</param>
        protected abstract void ProcessSocket(Handler.SocketHandle handle);

        /// <summary>
        /// The SocketHandleFactory to be used for new connections.
        /// </summary>
        protected Factory.SocketHandleFactory HandleFactory;

        /// <summary>
        /// Initializes the SocketController.
        /// </summary>
        /// <param name="handleFactory">The SocketHandleFactory to use for new connections.</param>
        public SocketController(Factory.SocketHandleFactory handleFactory)
        {
            Debug.Assert(handleFactory != null, "A SocketHandleFactory must be provided");
            HandleFactory = handleFactory;
        }

        /// <summary>
        /// Start listening on the specific DNS name and port.
        /// </summary>
        /// <param name="addr">The DNS address to resolve and bind to.</param>
        /// <param name="port">The port that should be bound to.</param>
        /// <param name="backlog">Optional - Max allowed number of pending connections.</param>
        public virtual void Listen(string addr, int port, int backlog = 1000)
        {
            Debug.Assert(addr != null, "Address cannot be null");

            IPAddress address = null;
            if (!IPAddress.TryParse(addr, out address))
            {
                IPHostEntry hostEntry = Dns.GetHostEntry(addr);
                if (hostEntry.AddressList.Length > 0)
                {
                    address = hostEntry.AddressList[0];
                }
            }

            Listen(address, port, backlog);
        }

        /// <summary>
        /// Start listening on the specific IP address and port.
        /// </summary>
        /// <param name="addr">The IP address to bind to.</param>
        /// <param name="port">The port that should be bound to.</param>
        /// <param name="backlog">Optional - Max allowed number of pending connections.</param>
        public virtual void Listen(IPAddress addr, int port, int backlog = 1000)
        {
            Debug.Assert(addr != null, "IP Address cannot be null");

            Socket socket = new Socket(addr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(new IPEndPoint(addr, port));
            socket.Listen(backlog);
            IsListening = true;
            socket.BeginAccept(new AsyncCallback(AcceptCallback), socket);
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            if (IsListening)
            {
                Socket client;
                Socket server = (Socket)ar.AsyncState;

                if (server != null)
                {
                    client = server.EndAccept(ar);
                    server.BeginAccept(new AsyncCallback(AcceptCallback), server);

                    Connections++;
                    Handler.SocketHandle handle = null;
                    try
                    {
                        handle = HandleFactory.CreateSocketHandle(client);
                        if (handle != null)
                        {
                            ProcessSocket(handle);
                        }
                    }
                    catch (IOException) { }
                    catch (AggregateException) { }

                    Connections--;
                    client.Disconnect(false);

                    if (handle != null)
                    {
                        handle.Dispose();
                    }
                }
            }
        }
    }
}