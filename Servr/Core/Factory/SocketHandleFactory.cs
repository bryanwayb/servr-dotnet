﻿using System.Net.Sockets;

namespace Servr.Core.Factory
{
    /// <summary>
    /// A factory for creating SocketHandle objects for basic Socket connections.
    /// </summary>
    public class SocketHandleFactory
    {
        /// <summary>
        /// Creates a new SocketHandle given a Socket object
        /// </summary>
        /// <param name="sock">The Socket object to use for the new SocketHandle</param>
        /// <returns>The new SocketHandle object</returns>
        public virtual Handler.SocketHandle CreateSocketHandle(Socket sock)
        {
            return new Handler.SocketHandle(sock);
        }
    }
}