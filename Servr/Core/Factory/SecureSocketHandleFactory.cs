﻿using System;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace Servr.Core.Factory
{
    /// <summary>
    /// A factory for creating SecureSocketHandle objects for SSL/TLS based connections.
    /// </summary>
    public class SecureSocketHandleFactory : SocketHandleFactory
    {
        /// <summary>
        /// The certificate to use for server authentication.
        /// </summary>
        public X509Certificate2 Certificate = null;

        /// <summary>
        /// Enables or disables the requirement for client certificates. Default is false.
        /// </summary>
        public bool RequireClientCert = false;

        /// <summary>
        /// If true, checks for revoked certificates from clients.
        /// </summary>
        public bool CheckCertificateRevoked = true;

        /// <summary>
        /// Enables and disables SSL/TLS protocols. Defaults are TLS 1.0, TLS 1.1, and TLS 1.2.
        /// </summary>
        public SslProtocols SSLProtocols = SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;

        /// <summary>
        /// How long to wait for the server to authenticate itself before giving up and disconnecting.
        /// </summary>
        public int AuthenticationTimeout = 120000;

        private RemoteCertificateValidationCallback certCallback = null;
        private LocalCertificateSelectionCallback localCertCallback = null;

        /// <summary>
        /// Initializes a new SecureSocketHandleFactory object with the given certificate callbacks.
        /// </summary>
        /// <param name="certCallback">The callback to use for certificate authentication. Can be null.</param>
        /// <param name="localCertCallback">The callback to use for local certificate authentication. Can be null.</param>
        public SecureSocketHandleFactory(RemoteCertificateValidationCallback certCallback, LocalCertificateSelectionCallback localCertCallback)
        {
            this.certCallback = certCallback;
            this.localCertCallback = localCertCallback;
        }

        /// <summary>
        /// Initializes a new SecureSocketHandleFactory object with the default cerfiticate callbacks.
        /// </summary>
        public SecureSocketHandleFactory() : this(null, null)
        {
        }

        /// <summary>
        /// Creates a new SecureSocketHandle given a Socket object
        /// </summary>
        /// <param name="sock">The Socket object to use for the new SecureSocketHandle</param>
        /// <returns>The new SocketHandle object</returns>
        public override Handler.SocketHandle CreateSocketHandle(Socket sock)
        {
            Handler.SecureSocketHandle ret = new Handler.SecureSocketHandle(sock, certCallback, localCertCallback);
            try
            {
                ret.SSL.AuthenticateAsServerAsync(Certificate, RequireClientCert, SSLProtocols, CheckCertificateRevoked).Wait(AuthenticationTimeout);
            }
            catch (OperationCanceledException) { }

            if (ret.SSL.IsAuthenticated && ret.SSL.IsEncrypted && ret.SSL.IsSigned)
            {
                return ret;
            }
            return null;
        }
    }
}