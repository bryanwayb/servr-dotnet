﻿using Servr.Core.Handler;
using Servr.HTTP.Engine;
using System.Diagnostics;

namespace Servr.HTTP.Modules.Rewrite
{
    public class Processor : Module
    {
        public override int Order
        {
            get
            {
                return Priority.Init;
            }
        }

        public Rule[] Rules = null;

        public Processor(Rule[] rules)
        {
            Debug.Assert(rules != null && rules.Length > 0, "Rewrite processor is missing rules");
            Rules = rules;
        }

        public override int Process(Settings settings, HTTPEngineBase engine, SocketHandle handle, ref Request request, ref Response response)
        {
            int retPriority = Priority.Netural;
            for (int i = 0; i < Rules.Length; i++)
            {
                Rule rule = Rules[i];
                if (rule.Expression.IsMatch(request.Uri) && (rule.Condition == null || rule.Condition(rule.Expression, ref request)))
                {
                    string location = rule.Replace(rule.Expression, ref request);
                    if (rule.ExternalRedirect)
                    {
                        retPriority = Priority.Output;
                        if(rule.StatusCode == 0)
                        {
                            rule.StatusCode = StatusCodes.TemporaryRedirect;
                        }
                        engine.ResponseRedirect(ref request, ref response, location, rule.StatusCode);
                        break;
                    }
                    else
                    {
                        request.Uri = location;
                    }
                }
            }

            return retPriority;
        }
    }
}