﻿using System;
using System.Text.RegularExpressions;

namespace Servr.HTTP.Modules.Rewrite
{
    public struct Rule
    {
        public delegate string ReplaceFunc(Regex regex, ref Request request);
        public delegate bool ConditionFunc(Regex regex, ref Request request);

        public string Match
        {
            get
            {
                return Expression.ToString();
            }
            set
            {
                Expression = new Regex(value, RegexOptions.Compiled);
            }
        }

        public Regex Expression;
        public ReplaceFunc Replace;
        public ConditionFunc Condition;
        public bool ExternalRedirect;
        public int StatusCode;
    }
}