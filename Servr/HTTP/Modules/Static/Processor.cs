﻿using Servr.Core.Handler;
using Servr.HTTP.Engine;
using System.Text;

namespace Servr.HTTP.Modules.Static
{
    public class Processor : Module
    {
        public override int Order
        {
            get
            {
                return Priority.Content;
            }
        }

        public override int Process(Settings settings, HTTPEngineBase engine, SocketHandle handle, ref Request request, ref Response response)
        {
            response.Status = StatusCodes.OK;
            engine.ResponseBasicPage(request, ref response, "Testing", "This is a test string: " + request.Uri);
            return Priority.Netural;
        }
    }
}