﻿using System.Collections.Generic;

namespace Servr.HTTP
{
    /// <summary>
    /// Structure representing a generic HTTP request not specific to any version.
    /// </summary>
    public struct Request
    {
        /// <summary>
        /// The Uri that was requested.
        /// </summary>
        public string Uri;

        /// <summary>
        /// The method that this request was sent with.
        /// </summary>
        public string Method;

        /// <summary>
        /// The headers contained with this request. All header names are lowercase.
        /// </summary>
        public Dictionary<string, string> Headers;

        /// <summary>
        /// The content contained within this request, as received.
        /// </summary>
        public byte[] Content;

        /// <summary>
        /// Flag that indicates no more processing should be handled on this request as it's either
        /// been handled already or the connection has been dropped.
        /// </summary>
        public bool Cancel;

        /// <summary>
        /// Flag for internal use indicating if the Socket connection should be persistent.
        /// </summary>
        public bool KeepAlive;
    }
}