﻿using System.Collections.Generic;

namespace Servr.HTTP
{
    /// <summary>
    /// Structure representing a generic HTTP response not specific to any version
    /// </summary>
    public struct Response
    {
        /// <summary>
        /// The response status code that should be sent back to the client
        /// </summary>
        public int Status;

        /// <summary>
        /// The response headers
        /// </summary>
        public Dictionary<string, string> Headers;

        /// <summary>
        /// The content payload as it should be sent
        /// </summary>
        public byte[] Content;

        /// <summary>
        /// When true indicates that more messages are to be sent to the client and to continue
        /// processing. Only valid for HTTP 1.1 and later.
        /// </summary>
        public bool MoreMessages;

        public byte[] CachedOutput;
    }
}