﻿using Servr.Core.Handler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Servr.HTTP
{
    /// <summary>
    /// A HTTP server.
    /// </summary>
    public class Controller : Core.SocketController
    {
        /// <summary>
        /// The Settings object that is used to control operation of this controller.
        /// </summary>
        public Settings Settings = null;

        /// <summary>
        /// The list of HTTP engines to use for handling connections. Defaults are HTTP/1.0 and HTTP/1.1.
        /// </summary>
        public Dictionary<string, Engine.HTTPEngineBase> Engines = new Dictionary<string, Engine.HTTPEngineBase>()
        {
            { Engine.HTTPEngine_1_0.Instance.Version, Engine.HTTPEngine_1_0.Instance },
            { Engine.HTTPEngine_1_1.Instance.Version, Engine.HTTPEngine_1_1.Instance }
        };

        private Engine.HTTPEngineBase DefaultHTTPEngine = null;

        private Module HTTPModule = null;

        /// <summary>
        /// Creates a new Controller object with the given SocketHandleFactory.
        /// </summary>
        /// <param name="handleFactory">The SocketHandleFactory to use for new connections.</param>
        public Controller(Core.Factory.SocketHandleFactory handleFactory) : this(new Settings()
        {
            HandleFactory = handleFactory
        })
        { }

        public Controller(Settings settings) : base(settings.GetHandleFactory())
        {
            Debug.Assert(settings != null, "A Settings object must be given");

            Debug.Assert(Engines.ContainsKey(settings.DefaultHTTPEngine), "The default HTTP engine specified does not exists");

            DefaultHTTPEngine = Engines[settings.DefaultHTTPEngine];
            Settings = settings;
        }

        /// <summary>
        /// Creates a new Controller object with a default SocketHandleFactory.
        /// </summary>
        public Controller() : this(new Core.Factory.SocketHandleFactory())
        {
        }

        public void AddModule(Module module)
        {
            Debug.Assert(module != null, "Cannot add a null module");
            Debug.Assert(module.Order > 0, "Cannot add a module with a priority of 0 or less");

            if (HTTPModule == null)
            {
                HTTPModule = module;
            }
            else
            {
                Module currentModule = HTTPModule;
                while (currentModule.Order <= module.Order)
                {
                    if (currentModule.Next == null)
                    {
                        break;
                    }
                    currentModule = currentModule.Next;
                }

                if (currentModule == HTTPModule)
                {
                    HTTPModule = module;
                    module.Next = currentModule;
                }
                else
                {
                    if (currentModule.Next != null)
                    {
                        module.Next = currentModule.Next;
                    }
                    currentModule.Next = module;
                }
            }
        }

        public void RemoveModule(Module module)
        {
            if (HTTPModule != null)
            {
                Module currentModule = HTTPModule,
                    lastModule = null;
                while (currentModule != null)
                {
                    if (currentModule == module)
                    {
                        if (lastModule == null)
                        {
                            HTTPModule = HTTPModule.Next;
                        }
                        else
                        {
                            lastModule.Next = currentModule.Next;
                        }
                        break;
                    }

                    currentModule = currentModule.Next;
                    lastModule = currentModule;
                }
            }
        }

        /// <summary>
        /// Processes HTTP requests from the client.
        /// </summary>
        /// <param name="handle">The SocketHandle to use for this connection.</param>
        protected override void ProcessSocket(SocketHandle handle)
        {
            handle.Sock.NoDelay = Settings.SocketDisableNagleAlgorithm;
            handle.Sock.ReceiveBufferSize = Settings.RequestBufferSize;
            handle.Sock.SendBufferSize = Settings.ResponseBufferSize;
            handle.Sock.SendTimeout = Settings.SocketSendTimeout;
            handle.Sock.ReceiveTimeout = Settings.SocketreceiveTimeout;

            if (Settings.SocketEnableKeepAlive)
            {
                handle.Sock.SetSocketOption(System.Net.Sockets.SocketOptionLevel.Socket, System.Net.Sockets.SocketOptionName.KeepAlive, true);
            }

            bool keepAlive = true;
            while (keepAlive)
            {
                byte[] currentBuffer = new byte[Settings.RequestBufferSize];
                int readSize = 0,
                    index,
                    lastIndex = 0,
                    trimIndex = 0,
                    requestLineIndex = 0,
                    trimLength;
                bool continueSearch,
                    moveNext,
                    shouldAppend = true,
                    expectingEOL = false;
                string[] requestLine = new string[3];
                StringBuilder sb = new StringBuilder(16, Settings.RequestLineTokenMaxLength);

                // I don't want to be tolerant to client requests in the request line.
                // So strictly following the standard here.
                while (shouldAppend && (readSize = handle.Read(currentBuffer, 0, 1)) > 0)
                {
                    lastIndex = trimIndex = index = 0;

                    continueSearch = true;
                    while (continueSearch)
                    {
                        moveNext = false;
                        shouldAppend = requestLineIndex < 3;
                        for (; (continueSearch = index < readSize && shouldAppend); index++)
                        {
                            if (requestLineIndex < 2)
                            {
                                if (currentBuffer[index] == ' ') // Only valid between request indexes 1 & 2
                                {
                                    moveNext = true;
                                    trimIndex = index;
                                    break;
                                }
                                else if (currentBuffer[index] == '\r' || currentBuffer[index] == '\n')
                                {
                                    DefaultHTTPEngine.WriteResponse(Settings, StatusCodes.GetStatusPage(null, DefaultHTTPEngine, StatusCodes.BadRequest), handle);
                                    return; // Bad request, kill the connection
                                }
                            }
                            else if (currentBuffer[index] == '\r')
                            {
                                expectingEOL = true;
                                trimIndex = index;
                            }
                            else if (currentBuffer[index] == '\n')
                            {
                                if (expectingEOL)
                                {
                                    moveNext = true;
                                    trimIndex = index - 1;
                                    expectingEOL = false;
                                    break;
                                }
                                else
                                {
                                    DefaultHTTPEngine.WriteResponse(Settings, StatusCodes.GetStatusPage(null, DefaultHTTPEngine, StatusCodes.BadRequest), handle);
                                    return; // Disconnect
                                }
                            }
                            else if (expectingEOL)
                            {
                                DefaultHTTPEngine.WriteResponse(Settings, StatusCodes.GetStatusPage(null, DefaultHTTPEngine, StatusCodes.BadRequest), handle);
                                return; // Disconnect
                            }
                        }

                        if (((shouldAppend && !((moveNext && trimIndex == 0))) || expectingEOL) && trimIndex >= 0)
                        {
                            trimLength = trimIndex - lastIndex;
                            if (trimLength <= 0)
                            {
                                trimLength = readSize - lastIndex;
                            }

                            if (trimLength > 0)
                            {
                                try
                                {
                                    sb.Append(Encoding.UTF8.GetString(currentBuffer, lastIndex, trimLength));
                                }
                                catch (ArgumentOutOfRangeException)
                                {
                                    int status;
                                    if (requestLineIndex == 1) // If we're parsing the Uri, send back that specific error
                                    {
                                        status = StatusCodes.URITooLong;
                                    }
                                    else // Otherwise it's just a general payload size
                                    {
                                        status = StatusCodes.PayloadTooLarge;
                                    }
                                    DefaultHTTPEngine.WriteResponse(Settings, StatusCodes.GetStatusPage(null, DefaultHTTPEngine, DefaultHTTPEngine.GetMappedStatusCode(status)), handle);
                                    return;
                                }
                            }
                        }

                        if (moveNext)
                        {
                            requestLine[requestLineIndex] = sb.ToString();
                            sb.Clear();
                            lastIndex = ++index;
                            requestLineIndex++;
                        }
                    }
                }

                sb = null;

                string httpVersion = requestLine[2];
                if (httpVersion != null)
                {
                    if (Engines.ContainsKey(httpVersion))
                    {
                        Engine.HTTPEngineBase engine = Engines[httpVersion];
                        Request req = engine.ReadRequest(Settings, requestLine[0], requestLine[1], currentBuffer, lastIndex, handle);

                        keepAlive = req.KeepAlive && Settings.HTTPEnableKeepAlive;

                        if (!req.Cancel)
                        {
                            Response res = new Response()
                            {
                                Headers = new Dictionary<string, string>()
                            };
                            do
                            {
                                engine.PrepareResponse(req, ref res, handle);

                                if (HTTPModule != null)
                                {
                                    bool cancelRequest = false;
                                    Module currentModule = HTTPModule;
                                    while (currentModule != null)
                                    {
                                        int adjustPriority = currentModule.Process(Settings, engine, handle, ref req, ref res);
                                        if (adjustPriority != Module.Priority.Netural)
                                        {
                                            if (adjustPriority == Module.Priority.Cancel)
                                            {
                                                cancelRequest = true;
                                                break;
                                            }
                                            else if (adjustPriority > currentModule.Order)
                                            {
                                                while (currentModule.Next != null)
                                                {
                                                    if (currentModule.Next.Order >= adjustPriority)
                                                    {
                                                        break;
                                                    }
                                                    currentModule = currentModule.Next;
                                                }
                                            }
                                        }
                                        currentModule = currentModule.Next;
                                    }
                                    if (cancelRequest)
                                    {
                                        break;
                                    }
                                }

                                engine.WriteResponse(Settings, res, handle);
                            } while (res.MoreMessages);
                        }
                    }
                    else
                    {
                        keepAlive = false;
                        DefaultHTTPEngine.WriteResponse(Settings, StatusCodes.GetStatusPage(null, DefaultHTTPEngine, DefaultHTTPEngine.GetMappedStatusCode(StatusCodes.HTTPVersionNotSupported)), handle);
                    }
                }
                else
                {
                    keepAlive = false;
                }
            }
        }
    }
}