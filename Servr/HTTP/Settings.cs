﻿using System;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace Servr.HTTP
{
    /// <summary>
    /// Settings to be applied to the HTTP server
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// The buffer size to allocate for reading request payloads. The default is 51200 (50KB).
        /// </summary>
        public int RequestBufferSize = 51200;

        /// <summary>
        /// The response buffer size. Doesn't do much other than set the Socket.SendBufferSize value.
        /// The default is 51200 (50KB).
        /// </summary>
        public int ResponseBufferSize = 51200;

        /// <summary>
        /// Defines the max length of values in the HTTP request line. This prevents malicious requests
        /// trying to pressure memory resources to their limits. This essentially limits the amount of
        /// characters available to use as a Uri (because that is the middle token of the request line).
        /// Therefore, the default is set to 2083, the max Url length for IE 8.
        /// </summary>
        public int RequestLineTokenMaxLength = 2083;

        /// <summary>
        /// Defines the max length of a header token in an HTTP request. A token is either the header name
        /// or the header value, so this value affects the max length of both. Default is 8192 (8KB).
        /// </summary>
        public int HeaderTokenMaxLength = 8192;

        /// <summary>
        /// The max size for the content body of a request message. This should be limited to only what is
        /// going to be required. Default is 49152 (48KB)
        /// </summary>
        public int RequestContentMaxSize = 49152;

        /// <summary>
        /// Enables or disables persistant connections on the HTTP level, default is true.
        /// </summary>
        public bool HTTPEnableKeepAlive = true;

        /// <summary>
        /// Enables or disables Socket level keep alive messages for long lasting connections. Default is false.
        /// </summary>
        public bool SocketEnableKeepAlive = false;

        /// <summary>
        /// Setting to true disables the Nagle Algorithm, default is false.
        /// </summary>
        public bool SocketDisableNagleAlgorithm = false;

        /// <summary>
        /// The max amount of time that the connection should wait (ms) when trying to send data over a Socket before
        /// killing the connection.
        /// </summary>
        public int SocketSendTimeout = 5000;

        /// <summary>
        /// The max amount of time that the connection should wait (ms) when trying to receive data over a Socket before
        /// killing the connection.
        /// </summary>
        public int SocketreceiveTimeout = 5000;

        /// <summary>
        /// The default HTTP engine to use for responses where a HTTP version could not be specified from the payload, or
        /// the HTTP version requested is not supported.
        /// </summary>
        public string DefaultHTTPEngine = Engine.HTTPEngine_1_0.Instance.Version;

        /// <summary>
        /// The SocketHandleFactory to use for new SocketHandle objects.
        /// </summary>
        public Core.Factory.SocketHandleFactory HandleFactory = null;

        /// <summary>
        /// Enables or disables SSL/TLS encryption. Changing this after being associated with a HTTP.Controller object requires
        /// the HTTP.Controller.HandleFactory field to be set manually, although that's likely not a very good idea unless
        /// you're a fan of hotswapping.
        /// </summary>
        public bool EnableSSL = false;

        /// <summary>
        /// Gets a SocketHandleFactory object based on the current configuration.
        /// </summary>
        /// <returns>The existing or generated SocketHandleFactory object</returns>
        public Core.Factory.SocketHandleFactory GetHandleFactory()
        {
            if (HandleFactory == null)
            {
                if (EnableSSL)
                {
                    HandleFactory = new Core.Factory.SecureSocketHandleFactory()
                    {
                        Certificate = SSLGetCertificate(),
                        AuthenticationTimeout = SSLAuthenticationTimeout,
                        CheckCertificateRevoked = SSLCheckRevokedCertificates,
                        RequireClientCert = SSLRequireClientCertificates,
                        SSLProtocols = SSLEnabledProtocols
                    };
                }
                else
                {
                    HandleFactory = new Core.Factory.SocketHandleFactory();
                }
            }
            return HandleFactory;
        }

        /// <summary>
        /// The certificate to use when SSL is enabled. Defaults to null.
        /// </summary>
        public X509Certificate2 SSLCertificate = null;

        /// <summary>
        /// THe SSL certificate file to load if SSL is enabled and SSLCertificate is null. Defaults to null.
        /// </summary>
        public string SSLCertificateFile = null;

        /// <summary>
        /// The password for the SSL certificate file, if one is required. Defaults to null.
        /// </summary>
        public string SSLCertificateFilePassword = null;

        /// <summary>
        /// The amount of time to wait before giving up on SSL authentication. Defaults to 5.
        /// </summary>
        public int SSLAuthenticationTimeout = 5;

        /// <summary>
        /// If true and SSLRequireClientCertificates is true this checked for revoked certificates. Defaults to false.
        /// </summary>
        public bool SSLCheckRevokedCertificates = false;

        /// <summary>
        /// This connection requires valid client certificates. Defaults to false.
        /// </summary>
        public bool SSLRequireClientCertificates = false;

        /// <summary>
        /// The SSL versions that should be allowed to connect.
        /// </summary>
        public SslProtocols SSLEnabledProtocols = SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;

        /// <summary>
        /// Gets the configured certificate to use for SSL or loads one if able to do so. Throws an
        /// Exception when not able to load a certificate from somewhere.
        /// </summary>
        /// <returns>The certificate to use.</returns>
        public X509Certificate2 SSLGetCertificate()
        {
            if (SSLCertificate == null)
            {
                if (!string.IsNullOrEmpty(SSLCertificateFile))
                {
                    if (string.IsNullOrEmpty(SSLCertificateFilePassword))
                    {
                        SSLCertificate = new X509Certificate2(SSLCertificateFile);
                    }
                    else
                    {
                        SSLCertificate = new X509Certificate2(SSLCertificateFile, SSLCertificateFilePassword);
                    }
                }
                else
                {
                    throw new Exception("Tried to get a certificate when one doesn't exist and no filepath to one given");
                }
            }
            return SSLCertificate;
        }
    }
}