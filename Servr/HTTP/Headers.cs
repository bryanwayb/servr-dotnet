﻿namespace Servr.HTTP
{
    public static class Headers
    {
        public const string ContentType = "content-type";
        public const string ContentLength = "content-length";
        public const string Connection = "connection";
        public const string UserAgent = "user-agent";
        public const string Location = "location";
    }
}