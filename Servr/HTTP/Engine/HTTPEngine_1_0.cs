﻿using Servr.Core.Handler;
using System;
using System.Collections.Generic;

namespace Servr.HTTP.Engine
{
    public class HTTPEngine_1_0 : HTTPEngineBase
    {
        public static HTTPEngine_1_0 Instance = new HTTPEngine_1_0();

        public HTTPEngine_1_0()
        {
            Version = "HTTP/1.0";

            RegisterStatusCode(200, "OK");
            RegisterStatusCode(201, "Created");
            RegisterStatusCode(202, "Accepted");
            RegisterStatusCode(204, "No Content");
            RegisterStatusCode(301, "Moved Permanently");
            RegisterStatusCode(302, "Moved Temporarily");
            RegisterStatusCode(304, "Not Modified");
            RegisterStatusCode(400, "Bad Request");
            RegisterStatusCode(401, "Unauthorized");
            RegisterStatusCode(403, "Forbidden");
            RegisterStatusCode(404, "Not Found");
            RegisterStatusCode(500, "Internal Server Error");
            RegisterStatusCode(501, "Not Implemented");
            RegisterStatusCode(502, "Bad Gateway");
            RegisterStatusCode(503, "Service Unavailable");

            CodeTranslationMap = new Dictionary<int, int>()
            {
                [StatusCodes.TemporaryRedirect] = StatusCodes.Found,
                [StatusCodes.PayloadTooLarge] = StatusCodes.InternalServerError,
                [StatusCodes.HTTPVersionNotSupported] = StatusCodes.NotImplemented
            };
        }

        protected override void PrepareRequest(Settings settings, Request request)
        {
            if (request.KeepAlive && request.Headers.ContainsKey(Headers.Connection))
            {
                if (request.Headers[Headers.Connection].Equals("keep-alive", StringComparison.OrdinalIgnoreCase))
                {
                    request.KeepAlive = true;
                }
            }
        }

        public override void PrepareResponse(Request request, ref Response response, SocketHandle handle)
        {
            if (request.KeepAlive)
            {
                response.Headers.Add(Headers.Connection, "keep-alive");
            }
        }
    }
}