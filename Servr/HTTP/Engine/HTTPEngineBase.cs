﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;

namespace Servr.HTTP.Engine
{
    /// <summary>
    /// The base HTTP engine for functionality that is shared across all HTTP versions.
    /// </summary>
    public abstract class HTTPEngineBase
    {
        /// <summary>
        /// The HTTP version that this class represents. To be set by inheriting classes.
        /// </summary>
        public string Version = null;

        /// <summary>
        /// Use this for adding CRLF to byte[] payloads. Reduces the overhead of creating a new byte[] object everytime a CRLF is needed.
        /// </summary>
        private static byte[] _CRLF = new byte[] { (byte)'\r', (byte)'\n' };

        /// <summary>
        /// A cache of status line responses. Use this to lookup the first line for response payloads instead of manually creating it
        /// each time during runtime.
        /// </summary>
        private Dictionary<int, byte[]> StatusLine = new Dictionary<int, byte[]>();

        /// <summary>
        /// The list of status code reasons (their descriptions, i.e. 404 = Not Found)
        /// </summary>
        public Dictionary<int, string> StatusReason = new Dictionary<int, string>();

        /// <summary>
        /// This is a compatibility map for new HTTP status codes. Codes that don't exist in newer HTTP versions but still need to return
        /// a response for server events should place the unsupported codes here as the key and the mapped codes as the value. This value
        /// isn't used internally in the HTTPEngineBase class, however it does get used by StatusCodes.
        /// </summary>
        internal Dictionary<int, int> CodeTranslationMap = null;

        /// <summary>
        /// To be overridden. Called during the ReadRequest method once a Request object had been created and HTTP version level logic needs
        /// to be performed before returning to the calling method.
        /// </summary>
        /// <param name="settings">The Settings object that is associated with the HTTP.Controller class.</param>
        /// <param name="request">The Request object.</param>
        protected abstract void PrepareRequest(Settings settings, Request request);

        /// <summary>
        /// To be overridden. Processes the request on the HTTP versions level, after the common HTTP processing has been completed
        /// but before the response has been started. This prepares the request for further processing by modules.
        /// </summary>
        /// <param name="req">Reference to the Request object.</param>
        /// <param name="res">Reference to the Response object.</param>
        /// <param name="handle">SocketHandle object.</param>
        public abstract void PrepareResponse(Request req, ref Response res, Core.Handler.SocketHandle handle);

        /// <summary>
        /// Begin processing a request from the client connection.
        /// </summary>
        /// <param name="settings">The Settings object that is associated with the HTTP.Controller class</param>
        /// <param name="method">The method that this request was created with.</param>
        /// <param name="uri">The Uri that this request is for.</param>
        /// <param name="buffer">The buffer to use. This buffer will be reused throughout the request process.</param>
        /// <param name="offset">The initial offset to start reading from this buffer.</param>
        /// <param name="handle">The SocketHandle object to use for the client connection.</param>
        /// <returns></returns>
        public Request ReadRequest(Settings settings, string method, string uri, byte[] buffer, int offset, Core.Handler.SocketHandle handle)
        {
            Request ret = new Request();
            ret.Method = method.ToLower();
            ret.Uri = WebUtility.UrlDecode(uri);
            ret.Headers = new Dictionary<string, string>();

            int index,
                trimIndex,
                lastIndex,
                bufferSize = buffer.Length,
                readSize = bufferSize,
                appendLength;
            bool continueRead = true,
                keyLookup = true,
                continueSearch,
                expectingEOL = false,
                currentSearchDone = false,
                suspectHeadEnd = false;
            string currentKey = null;

            StringBuilder sb = new StringBuilder(16, settings.HeaderTokenMaxLength);

            do
            {
                index = offset;
                continueSearch = true;
                while (continueSearch)
                {
                    lastIndex = index;
                    trimIndex = -1;
                    for (; index < readSize; index++)
                    {
                        if (keyLookup)
                        {
                            if (buffer[index] == ':')
                            {
                                keyLookup = false;
                                trimIndex = index++;
                                currentSearchDone = true;
                                break;
                            }
                            else if (suspectHeadEnd)
                            {
                                if (buffer[index] == '\n')
                                {
                                    continueRead = false;
                                    readSize = 0; // Short-circuit the continueSearch loop
                                    if (expectingEOL)
                                    {
                                        trimIndex = index - 1;
                                    }
                                    break;
                                }
                                else if (buffer[index] == '\r')
                                {
                                    expectingEOL = true;
                                }
                                else
                                {
                                    if (expectingEOL)
                                    {
                                        expectingEOL = false; // A malformed request, I'm not a fan of letting this pass...
                                    }
                                    if (suspectHeadEnd)
                                    {
                                        suspectHeadEnd = false;
                                    }
                                }
                            }
                        }
                        else if (buffer[index] == '\r')
                        {
                            expectingEOL = true;
                            if (index == 0)
                            {
                                trimIndex = 0;
                            }
                        }
                        else if (buffer[index] == '\n')
                        {
                            if (index == 0)
                            {
                                trimIndex = 0;
                                if (expectingEOL)
                                {
                                    expectingEOL = false;
                                }
                            }
                            else
                            {
                                if (expectingEOL)
                                {
                                    trimIndex = index - 1;
                                    expectingEOL = false;
                                }
                                else
                                {
                                    trimIndex = index;
                                }
                            }
                            currentSearchDone = true;
                            suspectHeadEnd = true;
                            keyLookup = true;
                            index++;
                            break;
                        }
                    }
                    continueSearch = index < readSize;

                    if (trimIndex == -1)
                    {
                        trimIndex = index;
                    }

                    appendLength = trimIndex - lastIndex;
                    if (appendLength > 0)
                    {
                        try
                        {
                            sb.Append(Encoding.UTF8.GetString(buffer, lastIndex, trimIndex - lastIndex));
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            WriteResponse(settings, StatusCodes.GetStatusPage(ret, this, GetMappedStatusCode(StatusCodes.PayloadTooLarge)), handle);
                            ret.Cancel = true;
                            continueRead = false;
                            break;
                        }
                    }

                    if (currentSearchDone)
                    {
                        if (keyLookup) // If keyLookup is true this means it was *just* flipped, so sb = header value
                        {
                            ret.Headers.Add(currentKey, sb.ToString().TrimStart(' ')); // Trim beginning spaces
                        }
                        else
                        {
                            currentKey = sb.ToString().ToLower(); // Keep all headers as lowercase, reduce overhead of case insensitive lookups
                        }

                        sb.Clear();
                        currentSearchDone = false;
                    }
                }

                offset = 0;
            } while (continueRead && (readSize = handle.Read(buffer, 0, 1)) > 0);

            if (!ret.Cancel)
            {
                PrepareRequest(settings, ret);

                // Read the content payload
                if (ret.Headers.ContainsKey(Headers.ContentLength))
                {
                    int contentLength = int.Parse(ret.Headers[Headers.ContentLength]);
                    if (contentLength > 0)
                    {
                        if (contentLength <= settings.RequestContentMaxSize)
                        {
                            ret.Content = new byte[contentLength];
                            if (handle.Read(ret.Content, 0, 1) != contentLength) // Read the content length, if there's a mismatch return 400
                            {
                                WriteResponse(settings, StatusCodes.GetStatusPage(ret, this, StatusCodes.BadRequest), handle);
                                ret.Cancel = true;
                            }
                        }
                        else
                        {
                            WriteResponse(settings, StatusCodes.GetStatusPage(ret, this, GetMappedStatusCode(StatusCodes.PayloadTooLarge)), handle);
                            ret.Cancel = true;
                        }
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// Sends back a response to the client.
        /// </summary>
        /// <param name="response">The Reponse object to use for the response payload.</param>
        /// <param name="handle">The SocketHandle object to write to.</param>
        public void WriteResponse(Settings settings, Response response, Core.Handler.SocketHandle handle)
        {
            try
            {
                if (response.CachedOutput == null)
                {
                    Debug.Assert(StatusLine.ContainsKey(response.Status), "An invalid status code for this HTTP engine version has been passed to be sent to the client");
                    handle.Write(StatusLine[response.Status]);

                    if (response.Headers != null)
                    {
                        foreach (KeyValuePair<string, string> kv in response.Headers)
                        {
                            handle.Write(Encoding.UTF8.GetBytes($"{kv.Key}: {kv.Value}\r\n"));
                        }
                        handle.Write(_CRLF);
                    }

                    if (response.Content != null)
                    {
                        handle.Write(response.Content);
                    }
                }
                else
                {
                    handle.Write(response.CachedOutput);
                }
            }
            catch (SocketException) { }
        }

        /// <summary>
        /// Writes a fully qualified HTTP message payload to the Response objects cache to be reused.
        /// </summary>
        /// <param name="response"></param>
        public void WriteResponseCached(ref Response response)
        {
            Debug.Assert(StatusLine.ContainsKey(response.Status), "An invalid status code for this HTTP engine version has been passed to be sent to the client");

            using (MemoryStream ms = new MemoryStream())
            {
                byte[] buffer = StatusLine[response.Status];
                ms.Write(buffer, 0, buffer.Length);

                if (response.Headers != null)
                {
                    foreach (KeyValuePair<string, string> kv in response.Headers)
                    {
                        buffer = Encoding.UTF8.GetBytes($"{kv.Key}: {kv.Value}\r\n");
                        ms.Write(buffer, 0, buffer.Length);
                    }
                    ms.Write(_CRLF, 0, _CRLF.Length);
                }

                if (response.Content != null)
                {
                    ms.Write(response.Content, 0, response.Content.Length);
                }

                response.CachedOutput = ms.ToArray();
            }
        }

        public int GetMappedStatusCode(int status)
        {
            if (CodeTranslationMap != null && CodeTranslationMap.ContainsKey(status))
            {
                status = CodeTranslationMap[status];
            }
            return status;
        }

        /// <summary>
        /// To be called in engine startup to register a new status code and the description. Creates a byte[] cache in
        /// a Dictionary object.
        /// </summary>
        /// <param name="code">The status code</param>
        /// <param name="reason">The status description</param>
        protected void RegisterStatusCode(int code, string reason)
        {
            StatusReason.Add(code, reason);
            StatusLine.Add(code, Encoding.UTF8.GetBytes($"{Version} {code} {reason}\r\n"));
        }

        private static string BasicPageTemplate = "<!DOCTYPE HTML PUBLIC \" -//IETF//DTD HTML 2.0//EN\"><html><head><title>{0}</title></head><body>{1}</body>" +
            $"<hr /><p style=\"font-style: italic;\">servr/{Assembly.GetAssembly(typeof(StatusCodes)).GetName().Version.ToString()}</p></body></html>";

        public void ResponseBasicPage(Request? req, ref Response res, string title, string body)
        {
            string content = string.Format(BasicPageTemplate, title, body);
            res.Content = Encoding.UTF8.GetBytes(content);
            res.Headers[Headers.ContentLength] = Encoding.UTF8.GetByteCount(content).ToString();
            res.Headers[Headers.ContentType] = MimeTypes.Html;
        }

        public void ResponseRedirect(ref Request req, ref Response res, string location, int status)
        {
            status = GetMappedStatusCode(status);
            if (req.Method != "head")
            {
                ResponseBasicPage(req, ref res, "Redirecting...", $"This page has been moved to: <a href=\"{location}\">{location}</a>");
            }
            res.Headers[Headers.Location] = location;
            res.Status = status;
        }
    }
}