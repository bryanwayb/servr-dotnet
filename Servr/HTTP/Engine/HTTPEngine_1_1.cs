﻿using Servr.Core.Handler;
using System;

namespace Servr.HTTP.Engine
{
    public class HTTPEngine_1_1 : HTTPEngineBase
    {
        public static HTTPEngine_1_1 Instance = new HTTPEngine_1_1();

        public HTTPEngine_1_1()
        {
            Version = "HTTP/1.1";

            RegisterStatusCode(100, "Continue");
            RegisterStatusCode(101, "Switching Protocols");
            RegisterStatusCode(200, "OK");
            RegisterStatusCode(201, "Created");
            RegisterStatusCode(202, "Accepted");
            RegisterStatusCode(203, "Non-Authoritative Information");
            RegisterStatusCode(204, "No Content");
            RegisterStatusCode(205, "Reset Content");
            RegisterStatusCode(206, "Partial Content");
            RegisterStatusCode(300, "Multiple Choices");
            RegisterStatusCode(301, "Moved Permanently");
            RegisterStatusCode(302, "Found");
            RegisterStatusCode(303, "See Other");
            RegisterStatusCode(304, "Not Modified");
            RegisterStatusCode(305, "Use Proxy");
            RegisterStatusCode(307, "Temporary Redirect");
            RegisterStatusCode(400, "Bad Request");
            RegisterStatusCode(401, "Unauthorized");
            RegisterStatusCode(402, "Payment Required");
            RegisterStatusCode(403, "Forbidden");
            RegisterStatusCode(404, "Not Found");
            RegisterStatusCode(405, "Method Not Allowed");
            RegisterStatusCode(406, "Not Acceptable");
            RegisterStatusCode(407, "Proxy Authentication Required");
            RegisterStatusCode(408, "Request Timeout");
            RegisterStatusCode(409, "Conflict");
            RegisterStatusCode(410, "Gone");
            RegisterStatusCode(411, "Length Required");
            RegisterStatusCode(412, "Precondition Failed");
            RegisterStatusCode(413, "Payload Too Large");
            RegisterStatusCode(414, "URI Too Long");
            RegisterStatusCode(415, "Unsupported Media Type");
            RegisterStatusCode(416, "Range Not Satisfiable");
            RegisterStatusCode(417, "Expectation Failed");
            RegisterStatusCode(426, "Upgrade Required");
            RegisterStatusCode(500, "Internal Server Error");
            RegisterStatusCode(501, "Not Implemented");
            RegisterStatusCode(502, "Bad Gateway");
            RegisterStatusCode(503, "Service Unavailable");
            RegisterStatusCode(504, "Gateway Timeout");
            RegisterStatusCode(505, "HTTP Version Not Supported");
        }

        protected override void PrepareRequest(Settings settings, Request request)
        {
            if (request.KeepAlive && request.Headers.ContainsKey(Headers.Connection))
            {
                if (request.Headers[Headers.Connection].Equals("close", StringComparison.OrdinalIgnoreCase))
                {
                    request.KeepAlive = false;
                }
            }
        }

        public override void PrepareResponse(Request request, ref Response response, SocketHandle handle)
        {
        }
    }
}