﻿using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Servr.HTTP
{
    /// <summary>
    /// Contains HTTP status codes and helper functions for responses.
    /// </summary>
    public static class StatusCodes
    {
        public const int Continue = 100;
        public const int SwitchingProtocols = 101;
        public const int OK = 200;
        public const int Created = 201;
        public const int Accepted = 202;
        public const int NonAuthoritativeInformation = 203;
        public const int NoContent = 204;
        public const int ResetContent = 205;
        public const int PartialContent = 206;
        public const int MultipleChoices = 300;
        public const int MovedPermanently = 301;
        public const int Found = 302;
        public const int SeeOther = 303;
        public const int NotModified = 304;
        public const int UseProxy = 305;
        public const int TemporaryRedirect = 307;
        public const int BadRequest = 400;
        public const int Unauthorized = 401;
        public const int PaymentRequired = 402;
        public const int Forbidden = 403;
        public const int NotFound = 404;
        public const int MethodNotAllowed = 405;
        public const int NotAcceptable = 406;
        public const int ProxyAuthenticationRequired = 407;
        public const int RequestTimeout = 408;
        public const int Conflict = 409;
        public const int Gone = 410;
        public const int LengthRequired = 411;
        public const int PreconditionFailed = 412;
        public const int PayloadTooLarge = 413;
        public const int URITooLong = 414;
        public const int UnsupportedMediaType = 415;
        public const int RangeNotSatisfiable = 416;
        public const int ExpectationFailed = 417;
        public const int UpgradeRequired = 426;
        public const int InternalServerError = 500;
        public const int NotImplemented = 501;
        public const int BadGateway = 502;
        public const int ServiceUnavailable = 503;
        public const int GatewayTimeout = 504;
        public const int HTTPVersionNotSupported = 505;

        private static Dictionary<int, Response> StatusPageTemplateCache = new Dictionary<int, Response>();

        /// <summary>
        /// Builds a Response object based on the HTTP engine and status code passed.
        /// </summary>
        /// <param name="engine">The HTTP engine to use for status code translation.</param>
        /// <param name="status">The status code that should be used to generate the response.</param>
        /// <returns>The Response object to send back to the client.</returns>
        public static Response GetStatusPage(Request? req, Engine.HTTPEngineBase engine, int status)
        {
            Response ret;
            lock (StatusPageTemplateCache)
            {
                if (!StatusPageTemplateCache.ContainsKey(status))
                {
                    ret = new Response()
                    {
                        Status = status,
                        Headers = new Dictionary<string, string>()
                    };
                    string line = $"{status} {engine.StatusReason[status]}";
                    engine.ResponseBasicPage(req, ref ret, line, $"<h1>${line}</h1>");
                    engine.WriteResponseCached(ref ret);
                    StatusPageTemplateCache.Add(status, ret);
                }
                else
                {
                    ret = StatusPageTemplateCache[status];
                }
            }

            return ret;
        }
    }
}