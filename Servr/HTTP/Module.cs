﻿namespace Servr.HTTP
{
    public abstract class Module
    {
        public static class Priority
        {
            public const int Cancel = -1;
            public const int Netural = 0;
            public const int Init = 1;
            public const int Content = 2;
            public const int Output = 3;
        }

        public abstract int Order { get; }

        public abstract int Process(Settings settings, Engine.HTTPEngineBase engine, Core.Handler.SocketHandle handle, ref Request request, ref Response response);

        /// <summary>
        /// For internal use only; holds reference to the next module in the execution chain.
        /// </summary>
        internal Module Next;
    }
}